This how-to is about getting a version of nmap on the Shark that supports using scripts.  

The nmap version that is available on the Shark is a "light" variant that doesn't support running scripts, something that vulners requires.  

Script features are available in the "nmap-full" package, but that hasn't been included in OpenWrt versions prior to 21.02  

It's possible to install nmap-full on the Shark, but it needs to be done in an "ugly" way that also might break other things. Or... there's no "might" in that really, cc-client (needed by Cloud C2 connectivity) will for sure break. It's of course possible to tweak things according to this how-to, use nmap, and then just factory reset the Shark when done to get back to a "safe state".  

To get nmap-full onboard the Shark, it's possible to do the following.  

This how-to was written using a factory reset battery based Shark with fw 1.1.0 (May 2024)  

**NOTE!** You are doing this at your own risk. If you manage to brick the Shark, it's your responsibility. This operation shouldn't be harmful though, and it's possible to do a firmware recovery to get the Shark back to original state. This will break some things on the Shark (such as cc-client), so be prepared that some things might not work as expected.  

There is some TLDR info at the end of this page that might be interesting to read, but not mandatory in order to pull this off.  

Put the Shark in arming mode and copy the following payload code into the `/root/payload/payload.sh` file (also available as a file in this repo)  

```bash
#!/bin/bash

LED SETUP
NETMODE DHCP_CLIENT
while ! ifconfig eth0 | grep "inet addr"; do sleep 1; done
/etc/init.d/sshd start
LED FINISH
```  

Sync and turn off the Shark  

Connect the Shark to a network port that provides internet access  

Boot the shark in attack mode  

ssh into the Shark from a device on the same network as the Shark is connected to  

A script that does all of what is needed "automatically" is available in the file: `nmap_vulners_shark.sh` Get that script onboard the Shark, make it executable and run it.  
**NOTE!!!** Read the script BEFORE using it and understand what it does! It's of course also possible to run each relevant command in the script manually  
The whole operation consumes about 10 MB of space on overlay (i.e. the root filesystem)  

When the script has finished, execute nmap to see if it works (it should)  


---

The TLDR section...  

It's not possible to only install the nmap-full the binary/package from 21.02.x even if downloading for the correct architecture. Mainly because of dependencies not having the correct versions  
These packages are required, but (as said) they are on other versions on 18.06.x  
libpcap1  
libstdcpp6  
libopenssl1.1  
liblua5.3-5.3  
libssh2-1  

Trying to "re-pack" the ipk and tell it to use packages that are available in 18.06.x is possible and installation of nmap-full is successful, but problems arise when trying to execute nmap (i.e. rename ipk to tar.gz, unpack the tar.gz, unpack the control.tar.gz, change the names of the required files/packages, re-pack the control.tar.gz, re-pack the nmap package, rename it back to ipk, install)  

Creating symlinks to the older equivalent packages/files isn't working either (sometimes it works, although a "risky" thing to do since it might break things in the same way as this "tweak" does)  

When it comes to the vulners scripts, there are of course other ways to get them on the Shark. It's possible to install git (and git-http) on the Shark, but it's not really necessary. Another way is to download the script files to some intermediate device and scp them to the Shark, but it's easier to just get them straight to the Shark from the source.  
