#!/bin/bash

/bin/wget http://www.example.com -qO /dev/null
RESULT=$?
if [ $RESULT -ne 0 ]; then
    /bin/echo "There is no internet connection! Exiting script..."
    exit
fi

/bin/mv /etc/opkg/distfeeds.conf /root/orig_df_conf.bak

/bin/echo "src/gz openwrt_core http://downloads.openwrt.org/releases/18.06.9/targets/ramips/mt76x8/packages" > /etc/opkg/distfeeds.conf
/bin/echo "src/gz openwrt_base http://downloads.openwrt.org/releases/18.06.9/packages/mipsel_24kc/base" >> /etc/opkg/distfeeds.conf
/bin/echo "src/gz openwrt_luci http://downloads.openwrt.org/releases/18.06.9/packages/mipsel_24kc/luci" >> /etc/opkg/distfeeds.conf
/bin/echo "src/gz openwrt_packages http://downloads.openwrt.org/releases/18.06.9/packages/mipsel_24kc/packages" >> /etc/opkg/distfeeds.conf
/bin/echo "src/gz openwrt_routing http://downloads.openwrt.org/releases/18.06.9/packages/mipsel_24kc/routing" >> /etc/opkg/distfeeds.conf
/bin/echo "src/gz openwrt_telephony http://downloads.openwrt.org/releases/18.06.9/packages/mipsel_24kc/telephony" >> /etc/opkg/distfeeds.conf

/bin/chmod 644 /etc/opkg/distfeeds.conf

/bin/opkg update

# Even though wget is already available on the Shark in /bin/wget, it has no SSL support
# Be sure to use /usr/bin/wget after installing
/bin/opkg install wget
/bin/opkg install unzip
/bin/mkdir /tmp/vulners
/bin/mkdir /root/nmap-vulners
/bin/mkdir /tmp/opkg_local

cd /tmp/opkg_local

/usr/bin/wget --no-check-certificate https://downloads.openwrt.org/releases/21.02.7/packages/mipsel_24kc/base/liblua5.3-5.3_5.3.5-4_mipsel_24kc.ipk
/usr/bin/wget --no-check-certificate https://downloads.openwrt.org/releases/21.02.7/packages/mipsel_24kc/base/libopenssl1.1_1.1.1t-2_mipsel_24kc.ipk
/usr/bin/wget --no-check-certificate https://downloads.openwrt.org/releases/21.02.7/packages/mipsel_24kc/base/libpcap1_1.9.1-3.1_mipsel_24kc.ipk
/usr/bin/wget --no-check-certificate https://downloads.openwrt.org/releases/21.02.7/packages/mipsel_24kc/packages/libssh2-1_1.9.0-2_mipsel_24kc.ipk
/usr/bin/wget --no-check-certificate https://downloads.openwrt.org/releases/21.02.7/targets/ramips/mt76x8/packages/libstdcpp6_8.4.0-3_mipsel_24kc.ipk
/usr/bin/wget --no-check-certificate https://downloads.openwrt.org/releases/21.02.7/packages/mipsel_24kc/packages/nmap-full_7.80-3_mipsel_24kc.ipk

/bin/opkg install ./liblua5.3-5.3_5.3.5-4_mipsel_24kc.ipk
/bin/opkg install ./libopenssl1.1_1.1.1t-2_mipsel_24kc.ipk
/bin/opkg --force-overwrite install ./libpcap1_1.9.1-3.1_mipsel_24kc.ipk
/bin/opkg --force-overwrite install ./libssh2-1_1.9.0-2_mipsel_24kc.ipk
/bin/opkg --force-overwrite install ./libstdcpp6_8.4.0-3_mipsel_24kc.ipk

/bin/opkg remove nmap

/bin/opkg install ./nmap-full_7.80-3_mipsel_24kc.ipk
# (takes a while)

/bin/rm /tmp/opkg_local/*.ipk
# (should remove themselves on the next boot of the Shark though)

cd /tmp/vulners
/usr/bin/wget --no-check-certificate https://github.com/vulnersCom/nmap-vulners/archive/refs/heads/master.zip

/usr/bin/unzip /tmp/vulners/master.zip

cp /tmp/vulners/nmap-vulners-master/*.nse /root/nmap-vulners/.
cp /tmp/vulners/nmap-vulners-master/*.txt /root/nmap-vulners/.
cp /tmp/vulners/nmap-vulners-master/*.json /root/nmap-vulners/.

cd /root/nmap-vulners

/usr/bin/clear

/bin/echo ""
/bin/echo ""
/bin/echo "nmap-full and vulners scripts should have been installed now"
/bin/echo "Try it using something like:"
/bin/echo "nmap --script /root/nmap-vulners/vulners -sV -p80 10.0.0.1"
/bin/echo ""
